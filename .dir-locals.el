((php-mode . (
	      (eval . (set (make-local-variable 'my-project-path) (locate-dominating-file default-directory ".dir-locals.el")))
              (flycheck-phpcs-standard-dir . my-project-path)
              (eval . (setq flycheck-phpmd-rulesets (expand-file-name (concat my-project-path "/.phpmd.xml"))))
              )))
