<?php

/**
 * Plugin Name: wp-ae-openid
 * Plugin URI: https://gitlab.com/animalequality/wp-openid
 * Description: Switch the login to OpenId Connect (exclusively or not).
 * Version: 0.3
 * Author: Raphaël Droz
 * Author URI: https://gitlab.com/animalequality
 * License: GPL-3.0
 *
 * This plugin lays on top of daggerhart/openid-connect-generic API
 * in order to allow fine-grained domain authorization/restrictions.
 * 1. It allows to drop the whole login/password WordPress UI, leaving
 *    only an "Connect with OpenId" button.
 *    This is the options `wp_ae_openid_only`
 * 2. It determines whether a user connecting using OpenId will be granted
 *    access or not.
 *    The option `wp_ae_openid_domains` is used for this purpose.
 *    It can be either a string, a regexp or an array of string and/or regexp.
 * 3. When redirecting to OpenId provider *and* only one domain is allowed,
 *    it use the OpenId "hd" parameter for convenience, as a hint for user about
 *    which domain it's expected to connect with.
 * 4. When used with Google OIDC, fixes user-data so email basename can be used
 *    as a login source rather than userdata provided by GSuite.
 */

namespace AnimalEquality\WP;

class OpenIdConnect
{
    private $openid_only = false;
    private $openid_domains = [];

    private static function bool_from_str($str)
    {
        return is_bool($str) ? $str : preg_match('/^(yes|y|1|true)$/i', $str);
    }

    private static function is_regexp($str)
    {
        return preg_match('/[\(\|\)\*]/', $str);
    }

    private function accepted_domain($hd)
    {
        foreach ($this->openid_domains as $allowed_domain) {
            if (self::is_regexp($allowed_domain)) {
                if (@preg_match('/' . $allowed_domain . '/', $hd)) {
                    return true;
                }
            } elseif ($hd === $allowed_domain) {
                return true;
            }
        }
        return false;
    }

    private function canonical_domain()
    {
        if (!$this->openid_domains || !is_array($this->openid_domains)) {
            return false;
        }
        if (count($this->openid_domains) != 1) {
            return false;
        }
        if (!isset($this->openid_domains[0]) || self::is_regexp($this->openid_domains[0])) {
            return false;
        }
        return $this->openid_domains[0];
    }

    public function login_accepted(bool $login_user, array $user_claim)
    {
        $login_user = false;

        if (isset($user_claim['hd'])
            && $user_claim['hd']
            && $user_claim['email_verified'] === true
            && $this->accepted_domain($user_claim['hd'])
        ) {
            $login_user = true;
        }

        return $login_user;
    }

    public static function ensure_preferred_name($claim)
    {
        if (!empty($claim['preferred_username'])) {
            return $claim;
        }

        $username = explode('@', $claim['email'])[0];
        $claim['preferred_username'] = $username;
        return $claim;
    }

    public static function fix_google_oidc(array $user_data, array $user_claim)
    {
        return array_merge(
            $user_data,
            ['display_name' => $user_claim['name'], 'first_name' => $user_claim['given_name']]
        );
    }

    public function __construct()
    {
        $this->openid_only = (bool)self::bool_from_str(get_option('wp_ae_openid_only', false));
        $this->openid_domains = get_option('wp_ae_openid_domains', []);
        if (is_string($this->openid_domains)) {
            $this->openid_domains = [ $this->openid_domains ];
        }

        if ($this->canonical_domain()) {
            add_filter(
                'openid-connect-generic-auth-url', function (string $url) {
                    return $url . '&hd=' . $this->canonical_domain();
                }
            );
        }

        // Request Google refresh_token so that we can renew them before they expire (in 3600s) without
        // going through the Google login-screen again.
        add_filter(
            'openid-connect-generic-auth-url', function (string $url) {
                return $url . '&access_type=offline&prompt=consent';
            }
        );

        // restrict who can register/login to the gsuite domain
        add_filter('openid-connect-generic-user-login-test', [ $this, 'login_accepted' ], 10, 2);

        add_filter('openid-connect-generic-alter-user-claim', [ __CLASS__, 'ensure_preferred_name' ]);
        // use the local part of the email as the username
        add_filter('openid-connect-generic-alter-user-data', [ __CLASS__, 'fix_google_oidc' ], 10, 2);

        // completely remove the usual form
        add_action(
            'login_enqueue_scripts', function () {
                if (class_exists('\OpenID_Connect_Generic')
                    && version_compare(\OpenID_Connect_Generic::VERSION, '3.4.0', '>=')
                    && $this->openid_only
                    && $this->openid_domains
                ) {
                    wp_add_inline_style('login', '#loginform, #nav { display:none; }');
                }
            }
        );
    }
}

$wp_ae_open = new OpenIdConnect();
